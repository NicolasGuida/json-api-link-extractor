import os

from googleapiclient.discovery import build
import json
import math
import pandas as pd

# Function that prints number of results
def print_results(obt_res):
    print("Il numero di risultati ottenuti dalla query è: ", obt_res)


# Function that appends links of a query to a list
def link_append(lis, qry_res):
    for result in qry_res['items']:
        link = result['link']
        lis.append(link)


def google_search(search_term: str, api_key: str, cse_id: str, **kwargs) -> json:
    """Perform a Google search using Custom Search API"""
    # Build request
    service = build("customsearch", "v1", developerKey=api_key)
    # Execute request
    query_result = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
    return query_result


# Initialize search parameters
my_api_key = os.environ.get("my_api_key")
my_cse_id = os.environ.get("my_cse_id")
search_term = os.environ.get("search_term")

# Execute first search
query_result = google_search(search_term, my_api_key, my_cse_id)
# Number of results
n_res = int(query_result['searchInformation']['totalResults'])

print_results(n_res)

# Output list
links_list = []

link_append(links_list, query_result)

# Number of requests
n_req = min(math.ceil(n_res / 10), 3) - 1

for i in range(1, n_req + 1):
    query_result = google_search(search_term, my_api_key, my_cse_id, start=1 + 10 * i)
    link_append(links_list, query_result)

# Output results as csv file
df = pd.DataFrame(links_list, columns=['Link'])
df = df.drop_duplicates(subset=None)
df.to_csv('link_estratti.csv', index=False)
